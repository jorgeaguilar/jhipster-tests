import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JhipsterTestsLocationModule } from './location/location.module';
import { JhipsterTestsDepartmentModule } from './department/department.module';
import { JhipsterTestsTaskModule } from './task/task.module';
import { JhipsterTestsEmployeeModule } from './employee/employee.module';
import { JhipsterTestsJobModule } from './job/job.module';
import { JhipsterTestsJobHistoryModule } from './job-history/job-history.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        JhipsterTestsLocationModule,
        JhipsterTestsDepartmentModule,
        JhipsterTestsTaskModule,
        JhipsterTestsEmployeeModule,
        JhipsterTestsJobModule,
        JhipsterTestsJobHistoryModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterTestsEntityModule {}
