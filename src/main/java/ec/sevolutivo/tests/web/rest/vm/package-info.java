/**
 * View Models used by Spring MVC REST controllers.
 */
package ec.sevolutivo.tests.web.rest.vm;
