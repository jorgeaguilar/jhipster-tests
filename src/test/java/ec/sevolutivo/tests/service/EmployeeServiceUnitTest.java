package ec.sevolutivo.tests.service;

import ec.sevolutivo.tests.domain.Employee;
import ec.sevolutivo.tests.domain.Job;
import ec.sevolutivo.tests.domain.JobHistory;
import ec.sevolutivo.tests.repository.EmployeeRepository;
import ec.sevolutivo.tests.repository.JobHistoryRepository;
import ec.sevolutivo.tests.web.rest.EmployeeResourceIntTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * Created by jorge on 11/24/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceUnitTest {
    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    JobHistoryService jobHistoryService;

    EmployeeService employeeService;


    @Before
    public void setUp() {
        employeeService = new EmployeeService(employeeRepository, jobHistoryService);
    }

    @Test
    public void shouldSaveJobHistoryWhenCreateAnEmployee() {
        Employee employee = EmployeeResourceIntTest.createEmployee1();
        Job job = new Job();
        job.setJobTitle("Supervisor");
        employee.setJob(job);

        employeeService.save(employee);
        JobHistory jobHistory = new JobHistory();
        ArgumentCaptor<JobHistory> argumentCaptor = ArgumentCaptor.forClass(JobHistory.class);
        Mockito.verify(jobHistoryService).save(argumentCaptor.capture());
        Assert.assertEquals(employee.getFirstName(),argumentCaptor.getValue().getEmployee().getFirstName());
        Assert.assertNotNull(argumentCaptor.getValue().getStartDate());
        Assert.assertEquals(job.getJobTitle(),argumentCaptor.getValue().getJob().getJobTitle());

    }
}
