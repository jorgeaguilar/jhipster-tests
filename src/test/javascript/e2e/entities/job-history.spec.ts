import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('JobHistory e2e test', () => {

    let navBarPage: NavBarPage;
    let jobHistoryDialogPage: JobHistoryDialogPage;
    let jobHistoryComponentsPage: JobHistoryComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load JobHistories', () => {
        navBarPage.goToEntity('job-history');
        jobHistoryComponentsPage = new JobHistoryComponentsPage();
        expect(jobHistoryComponentsPage.getTitle()).toMatch(/jhipsterTestsApp.jobHistory.home.title/);

    });

    it('should load create JobHistory dialog', () => {
        jobHistoryComponentsPage.clickOnCreateButton();
        jobHistoryDialogPage = new JobHistoryDialogPage();
        expect(jobHistoryDialogPage.getModalTitle()).toMatch(/jhipsterTestsApp.jobHistory.home.createOrEditLabel/);
        jobHistoryDialogPage.close();
    });

    it('should create and save JobHistories', () => {
        jobHistoryComponentsPage.clickOnCreateButton();
        jobHistoryDialogPage.setStartDateInput('2000-12-31');
        expect(jobHistoryDialogPage.getStartDateInput()).toMatch('2000-12-31');
        jobHistoryDialogPage.setEndDateInput('2000-12-31');
        expect(jobHistoryDialogPage.getEndDateInput()).toMatch('2000-12-31');
        jobHistoryDialogPage.jobSelectLastOption();
        jobHistoryDialogPage.departmentSelectLastOption();
        jobHistoryDialogPage.employeeSelectLastOption();
        jobHistoryDialogPage.save();
        expect(jobHistoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class JobHistoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-job-history div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class JobHistoryDialogPage {
    modalTitle = element(by.css('h4#myJobHistoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    startDateInput = element(by.css('input#field_startDate'));
    endDateInput = element(by.css('input#field_endDate'));
    jobSelect = element(by.css('select#field_job'));
    departmentSelect = element(by.css('select#field_department'));
    employeeSelect = element(by.css('select#field_employee'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setStartDateInput = function (startDate) {
        this.startDateInput.sendKeys(startDate);
    }

    getStartDateInput = function () {
        return this.startDateInput.getAttribute('value');
    }

    setEndDateInput = function (endDate) {
        this.endDateInput.sendKeys(endDate);
    }

    getEndDateInput = function () {
        return this.endDateInput.getAttribute('value');
    }

    jobSelectLastOption = function () {
        this.jobSelect.all(by.tagName('option')).last().click();
    }

    jobSelectOption = function (option) {
        this.jobSelect.sendKeys(option);
    }

    getJobSelect = function () {
        return this.jobSelect;
    }

    getJobSelectedOption = function () {
        return this.jobSelect.element(by.css('option:checked')).getText();
    }

    departmentSelectLastOption = function () {
        this.departmentSelect.all(by.tagName('option')).last().click();
    }

    departmentSelectOption = function (option) {
        this.departmentSelect.sendKeys(option);
    }

    getDepartmentSelect = function () {
        return this.departmentSelect;
    }

    getDepartmentSelectedOption = function () {
        return this.departmentSelect.element(by.css('option:checked')).getText();
    }

    employeeSelectLastOption = function () {
        this.employeeSelect.all(by.tagName('option')).last().click();
    }

    employeeSelectOption = function (option) {
        this.employeeSelect.sendKeys(option);
    }

    getEmployeeSelect = function () {
        return this.employeeSelect;
    }

    getEmployeeSelectedOption = function () {
        return this.employeeSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
